package fr.formation.tp9;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.SeekBar;

public class Secondaire extends Traceur {

    private static final String PREFS = "PROGRESS";

    SharedPreferences sharedPreferences;
    SeekBar seekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondaire);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        chargerParametres();
    }

    private void chargerParametres() {
        sharedPreferences = getSharedPreferences("TP9", MODE_PRIVATE);
        if (sharedPreferences.contains(PREFS)) {
            int progress = sharedPreferences.getInt(PREFS, 0);
            seekBar.setProgress(progress);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sauvegarderParametres();
    }

    private void sauvegarderParametres() {
       // sharedPreferences = getSharedPreferences("TP9", MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt(PREFS,seekBar.getProgress());
        edit.commit();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        int progress = savedInstanceState.getInt("progress", 0);
        seekBar.setProgress(progress);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar);
        int progress = seekBar.getProgress();
        Log.d("ETAT", "seekBar = " + progress);
        outState.putInt("progress", progress);
    }

}
