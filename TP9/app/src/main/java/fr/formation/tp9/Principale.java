package fr.formation.tp9;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Principale extends Traceur {

    private String valeur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principale);
    }

    public void onClick(View view) {
        Intent intent = new Intent(this, Secondaire.class);
        startActivity(intent);
    }
}
