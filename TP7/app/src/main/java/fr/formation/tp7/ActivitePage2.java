package fr.formation.tp7;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ActivitePage2 extends Activity {

    private VraiFaux[] questions = new VraiFaux[]{
            new VraiFaux("la nuit, ", "il fait jour", "il fait noir", false),
            new VraiFaux("Java est un langage", "à prototype", "objet", true),
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activite_page2);
        //questions[0].getQuestion();
        // On récupère l'intent qui a lancé cette activité
        Intent intent = getIntent();
        String texte = intent.getStringExtra("param");
        TextView editText = (TextView) findViewById(R.id.editText);
        editText.setText(texte);

    }
}
