/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.formation.tp16;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ListeUtilisateurFragment extends Fragment {

    private RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_utilisateurs, container, false);
        recyclerView = (RecyclerView) view;
        recyclerView.setAdapter(new RecyclerView.Adapter<UtilisateurViewHolder>() {
            @Override
            public UtilisateurViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.utilisateur_list_content, parent, false);
                return new UtilisateurViewHolder(view);

            }

            @Override
            public void onBindViewHolder(UtilisateurViewHolder holder, final int position) {
                holder.mContentView.setText(Mock.Titre[position]);

                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        UtilisateurFragment articleFrag = (UtilisateurFragment)
                                getFragmentManager().findFragmentById(R.id.article_fragment);

                        if (articleFrag != null) {
                            articleFrag.updateArticleView(position);

                        } else {
                            UtilisateurFragment newFragment = new UtilisateurFragment();
                            Bundle args = new Bundle();
                            args.putInt(UtilisateurFragment.ARG_POSITION, position);
                            newFragment.setArguments(args);
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
                            transaction.replace(R.id.fragment_container, newFragment);
                            transaction.addToBackStack(null);
                            transaction.commit();
                        }
                    }
                });
            }

            @Override
            public int getItemCount() {
                return Mock.Titre.length;
            }
        });
        return view;
    }


    public class UtilisateurViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mContentView;

        public UtilisateurViewHolder(View view) {
            super(view);
            mView = view;
            mContentView = (TextView) view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }

}