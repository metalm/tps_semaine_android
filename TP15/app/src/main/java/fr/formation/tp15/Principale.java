package fr.formation.tp15;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

public class Principale extends Activity {

    private ProgressBar bar;
    private Button button;
    private Traitement traitement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principale);

        bar = (ProgressBar) findViewById(R.id.progressBar);
        button = (Button) findViewById(R.id.toggleButton);

        traitement = new Traitement();
        traitement.execute();

        button.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                traitement.cancel(true);
            }
        });
    }

    class Traitement extends AsyncTask<Void, Integer, String> {

        @Override
        protected void onPreExecute() {
            Toast.makeText(getApplicationContext(), "Démarrage", Toast.LENGTH_LONG).show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            bar.setProgress(values[0]);
        }

        @Override
        protected String doInBackground(Void... arg0) {

            int progress;
            for (progress = 0; progress <= 100; progress++) {
                if (isCancelled()) break;
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
                publishProgress(progress);
            }
            return " , dong !";
        }

        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(getApplicationContext(), "Ding !" + result, Toast.LENGTH_LONG).show();
            //toast.setGravity(Gravity.CENTER, 0, 0)
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(getApplicationContext(), "D'oh !", Toast.LENGTH_LONG).show();
        }
    }
}

