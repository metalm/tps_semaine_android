/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.formation.tp18;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fr.formation.tp18.outils.Mock;

import static fr.formation.tp18.R.id.viewPager;

public class UtilisateurFragment extends Fragment {
    final static String ARG_POSITION = "position";
    private int mCurrentPosition = -1;
    private ViewPager pager;
    private ListeUtilisateursFragment liste;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
        }

        View view = inflater.inflate(R.layout.utilisateur, container, false);
        pager = (ViewPager) view.findViewById(viewPager);
        pager.setAdapter(new UtilisateurPagerAdapter(getChildFragmentManager()));
        pager.setCurrentItem(mCurrentPosition);

        liste = (ListeUtilisateursFragment) getFragmentManager().findFragmentById(R.id.liste_fragment);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.d("PAGER", "page : " + position);
                //View v = (View) liste.getAdapter().notifyDataSetChanged();

            }

            @Override
            public void onPageSelected(int position) {
                Log.d("PAGER", "page : " + position);
                //liste.getListView().setItemChecked(position, true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        pager.getAdapter().notifyDataSetChanged();
    }


    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        if (args != null) {
            updateArticleView(args.getInt(ARG_POSITION));
        } else if (mCurrentPosition != -1) {
            updateArticleView(mCurrentPosition);
        }
    }

    public void updateArticleView(int position) {
        if (pager != null) {
            pager.setCurrentItem(position);
            Log.d("SELECT PAGE", "" + position);
            //article.setText(Mock.utilisateurs.get(position).getDetail());
            mCurrentPosition = position;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(ARG_POSITION, mCurrentPosition);
    }

    ViewPager getPager() {
        return pager;
    }

    private class UtilisateurPagerAdapter extends FragmentPagerAdapter {

        public UtilisateurPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            return UtilisateurPagerFragment.newInstance(
                    Mock.utilisateurs.get(pos).getDetail()
            );
        }

        @Override
        public int getCount() {
            return Mock.utilisateurs.size();
        }
    }
}