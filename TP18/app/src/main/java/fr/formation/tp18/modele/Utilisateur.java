package fr.formation.tp18.modele;

/**
 * Created by ronan on 06/06/2017.
 */

public class Utilisateur {

    private String nom;
    private String detail;

    public Utilisateur() {

    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String toString() {
        return nom;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
