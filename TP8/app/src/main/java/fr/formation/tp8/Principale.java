package fr.formation.tp8;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Principale extends Activity {
    private EditText numero;
    private EditText text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principale);

        numero = (EditText)findViewById(R.id.editNumero);
        text = (EditText)findViewById(R.id.editText);
    }

    public void envoyer(View view) {
        String uri = "smsto:" +  numero.getText().toString();
        Intent intent = new Intent(android.content.Intent.ACTION_SENDTO,
                Uri.parse(uri));
        intent.putExtra("sms_body", text.getText().toString());
        startActivity(intent);
    }

    public void envoyerDirectementSMS () {
        // SmsManager smsManager = SmsManager.getDefault();
        // smsManager.sendTextMessage( numero.getText().toString() , null, text.getText().toString(), null, null);
        // Avant la balise application dans le manifest <uses-permission android:name="android.permission.SEND_SMS" />
    }
}
