package fr.formation.tp14;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ProgressBar;

import java.util.concurrent.atomic.AtomicBoolean;

public class Principale extends Activity {

    private static final String PROGRESS = "PROGRESS";
    private static final int PROGRESSION = 1;
    private ProgressBar bar;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == PROGRESSION) {
                int progress = msg.getData().getInt(PROGRESS);
                bar.setProgress(progress);
            }
        }
    };
    private AtomicBoolean isRunning = new AtomicBoolean(false);
    private AtomicBoolean isPausing = new AtomicBoolean(false);

    Thread traitement = new Thread(new Runnable() {

        Bundle messageBundle = new Bundle();
        Message message;

        public void run() {
            try {
                for (int i = 0; i <= 100 && isRunning.get(); i++) {
                    while (isPausing.get() && (isRunning.get())) {
                        Thread.sleep(1000);
                    }
                    Thread.sleep(300);
                    // On recupère un message:
                    message = handler.obtainMessage();
                    message.what = PROGRESSION;
                    messageBundle.putInt(PROGRESS, i);
                    message.setData(messageBundle);
                    handler.sendMessage(message);
                }
            } catch (Throwable t) {
            }
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principale);
        bar = (ProgressBar) findViewById(R.id.progressBar);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                bar.setMax(100);
            }
        });
    }

    public void onStart() {
        super.onStart();
        bar.setProgress(0);
        isRunning.set(true);
        isPausing.set(false);

        // On démarre notre Thread
        traitement.start();


    }

    public void onStop() {
        super.onStop();
        isRunning.set(false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        isPausing.set(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isPausing.set(false);
    }
}
