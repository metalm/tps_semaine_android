package fr.formation.tp10;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class Principale extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principale);
    }

    public void ajouterNombre(View view) {
        EditText tableau = (EditText)findViewById(R.id.tableau);
        Button button = (Button) view;
        tableau.setText(tableau.getText().toString() + button.getText());
    }

    public void push(View view) {
        push();
    }

    public void plus(View view) {
        push();
        EditText pile2 = (EditText)findViewById(R.id.pile2);
        EditText pile1 = (EditText)findViewById(R.id.pile1);
        String valeur1 = pile1.getText().toString();
        String valeur2 = pile2.getText().toString();
        if ( valeur1.length()>0 && valeur2.length()>0) {
            pop();
            pop();
            int v1 = new BigDecimal(valeur1).intValue();
            int v2 = new BigDecimal(valeur2).intValue();
            push(String.valueOf(v1+v2));
        }
    }

    public void clear(View view) {
        EditText pile4 = (EditText)findViewById(R.id.pile4);
        EditText pile3 = (EditText)findViewById(R.id.pile3);
        EditText pile2 = (EditText)findViewById(R.id.pile2);
        EditText pile1 = (EditText)findViewById(R.id.pile1);
        EditText tableau = (EditText)findViewById(R.id.tableau);
        pile1.setText("");
        pile2.setText("");
        pile3.setText("");
        pile4.setText("");
        tableau.setText("");
    }

    public void swap(View view) {
        push();
        EditText pile2 = (EditText)findViewById(R.id.pile2);
        EditText pile1 = (EditText)findViewById(R.id.pile1);
        String valeur1 = pile1.getText().toString();
        String valeur2 = pile2.getText().toString();
        if ( valeur1.length()>0 && valeur2.length()>0) {
            String tampon = pile1.getText().toString();
            pile1.setText(pile2.getText());
            pile2.setText(tampon);
        }
    }

    public void moins(View view) {
        push();
        EditText pile2 = (EditText)findViewById(R.id.pile2);
        EditText pile1 = (EditText)findViewById(R.id.pile1);
        String valeur1 = pile1.getText().toString();
        String valeur2 = pile2.getText().toString();
        if ( valeur1.length()>0 && valeur2.length()>0) {
            pop();
            pop();
            int v1 = new BigDecimal(valeur1).intValue();
            int v2 = new BigDecimal(valeur2).intValue();
            push(String.valueOf(v2-v1));
        }
    }

    public void divise(View view) {
        push();
        EditText pile2 = (EditText)findViewById(R.id.pile2);
        EditText pile1 = (EditText)findViewById(R.id.pile1);
        String valeur1 = pile1.getText().toString();
        String valeur2 = pile2.getText().toString();
        if ( valeur1.length()>0 && valeur2.length()>0) {
            pop();
            pop();
            int v1 = new BigDecimal(valeur1).intValue();
            int v2 = new BigDecimal(valeur2).intValue();
            push(String.valueOf(v2/v1));
        }
    }

    public void pop(View view) {
        push();
        pop();
    }

    private void pop() {
        EditText pile4 = (EditText)findViewById(R.id.pile4);
        EditText pile3 = (EditText)findViewById(R.id.pile3);
        EditText pile2 = (EditText)findViewById(R.id.pile2);
        EditText pile1 = (EditText)findViewById(R.id.pile1);

        pile1.setText(pile2.getText());
        pile2.setText(pile3.getText());
        pile3.setText(pile4.getText());
        pile4.setText("");
    }

    private void push() {
        EditText tableau = (EditText)findViewById(R.id.tableau);
        final String valeur = tableau.getText().toString();
        tableau.setText("");
        push(valeur);
    }

    private void push(final String pvaleur) {
        if (pvaleur != null && pvaleur.length()>0) {
            EditText pile4 = (EditText)findViewById(R.id.pile4);
            EditText pile3 = (EditText)findViewById(R.id.pile3);
            EditText pile2 = (EditText)findViewById(R.id.pile2);
            EditText pile1 = (EditText)findViewById(R.id.pile1);
            pile4.setText(pile3.getText());
            pile3.setText(pile2.getText());
            pile2.setText(pile1.getText());
            pile1.setText(pvaleur);
        }
    }

}
