package fr.formation.tp17;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import fr.formation.tp17.modele.Utilisateur;
import fr.formation.tp17.outils.Mock;

public class AjouterUtilisateurActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
    }

    public void sauvegarder(View v) {

        //TODO Stocker l'utilisateur dans une base de données (reprendre le projet TDXX).

        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setNom("a");
        utilisateur.setDetail("bla bla");

        Mock.utilisateurs.add(utilisateur);

        Intent resultIntent = new Intent();
        resultIntent.putExtra("valeur", "foo");

        setResult(Activity.RESULT_OK, resultIntent);

        finish();
    }
}
